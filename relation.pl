:- module(relation,
          [rel_create/1,
           rel_create/2,
           rel_create/3,
           rel_insert/3,
           rel_insert_many/3,
           rel_count/2,
           rel_query/2,
           rel_delete/3,
           rel_select/3]).

:- use_module(library(assoc)).
:- use_module(library(option)).
:- use_module(library(lists)).

%! rel_create(-Relation:rel) is det
%! rel_create(+Initial:list, -Relation:rel) is det
%! rel_create(+Initial:list, +Options:list, -Relation:rel) is det
%
%  Creates a relational value.

rel_create(Relation) :- rel_create([], Relation).
rel_create(Initial, Relation) :-
        rel_create(Initial, [], Relation).
rel_create(Initial, Options, Relation) :-
        empty_assoc(Tuples),
        option(coldefs(ColDefs), Options, []),
        length(ColDefs, Arity),
        maplist(functor, ColDefs, Names, _),
        maplist(arg(1), ColDefs, Preds),
        option(pks(PKs), Options, []),
        maplist(nth0_reorder(Names), PKs, PKIdxs0),
        option(pkidxs(PKIdxs), Options, PKIdxs0),
        EmptyRel = rel(meta(Arity, Names, Preds, PKIdxs), Tuples),
        rel_insert_many(EmptyRel, Initial, Relation).

% metadata getters
meta_arity(meta(Arity,_,_,_), Arity).
meta_names(meta(_,Names,_,_), Names).
meta_preds(meta(_,_,Preds,_), Preds).
meta_pkidxs(meta(_,_,_,PKIdxs), PKIdxs).

% insert

check_tuple(Meta, Tuple) :-
        meta_arity(Meta, Arity),
        length(Tuple, Length),
        (Arity = 0; Length = Arity).

key_from_tuple(Meta, Tuple, Key) :-
        meta_pkidxs(Meta, PKIdxs),
        (length(PKIdxs, 0) ->
         Key = Tuple;
         maplist(nth0_reorder(Tuple), Key, PKIdxs)).

%! rel_insert(+Relation0:rel, +Tuple:list, -Relation:rel) is det

rel_insert(Relation0, Tuple, Relation) :-
        rel(Meta, Tuples0) = Relation0,
        enforce_pred(ground(Tuple)),
        enforce_pred(check_tuple(Meta, Tuple)),
        key_from_tuple(Meta, Tuple, Key),
        put_assoc(Key, Tuples0, Tuple, Tuples),
        Relation = rel(Meta, Tuples).

%! rel_insert(+Relation0:rel, +Tuple:list, -Relation:rel) is det

rel_insert_many(Relation, [], Relation) :- !.
rel_insert_many(Relation0, [Tuple|Tuples], Relation) :-
        rel_insert(Relation0, Tuple, Relation1),
        rel_insert_many(Relation1, Tuples, Relation).

%! rel_count(+Relation:rel, -Count:int) is det
%! rel_count(+Relation:rel, +Count:int) is semidet

rel_count(rel(_, Tuples), Count) :-
        assoc_to_keys(Tuples, Keys),
        length(Keys, Count).

%! rel_query(+Relation0:rel, +Pattern) is nondet

rel_query(Relation, Pattern) :-
        rel(Meta, Tuples) = Relation,
        key_from_tuple(Meta, Pattern, Key),
        (ground(Key) ->
         get_assoc(Key, Tuples, Pattern);
         gen_assoc(Key, Tuples, Pattern)).

%! rel_delete(+Relation0:rel, +Tuple:list, -Relation:rel)

rel_delete(Relation0, Tuple, Relation) :-
        rel(Meta, Tuples0) = Relation0,
        key_from_tuple(Meta, Tuple, Key),
        (ground(Key) ->
         ((del_assoc(Key, Tuples0, Value, Tuples),Tuple=Value) ->
          Relation = rel(Meta, Tuples);
          Relation = Relation0);
         (findall(Value,
                  (assoc_to_values(Tuples0, Values0),
                   member(Value, Values0),
                   Tuple\=Value),
                  Values),
          empty_assoc(EmptyAssoc),
          Relation1 = rel(Meta, EmptyAssoc),
          rel_insert_many(Relation1, Values, Relation))).

%! rel_select(+Relation0:rel, +Tuple:list, -Relation:rel)

rel_select(Relation0, Tuple, Relation) :-
        rel(Meta, Tuples0) = Relation0,
        key_from_tuple(Meta, Tuple, Key),
        (ground(Key) ->
         ((get_assoc(Key, Tuples0, Value),Tuple=Value) ->
          (empty_assoc(EmptyAssoc),
           put_assoc(Key, EmptyAssoc, Value, OneAssoc),
           Relation = rel(Meta, OneAssoc));
          (empty_assoc(EmptyAssoc),
           Relation = rel(Meta, EmptyAssoc)));
         (findall(Value,
                  (assoc_to_values(Tuples0, Values0),
                   member(Value, Values0),
                   Tuple=Value),
                  Values),
          empty_assoc(EmptyAssoc),
          Relation1 = rel(Meta, EmptyAssoc),
          rel_insert_many(Relation1, Values, Relation))).

% helpers
enforce_pred(Pred) :- call(Pred),!.
enforce_pred(Pred) :- throw(['Predicate failed: ', Pred]).

nth0_reorder(List, Elem, Index) :- nth0(Index, List, Elem),!.

:- begin_tests(relation).

test_rel(R) :-
        rel_create([[a,b,c],[d,e,f]],
                   [coldefs([col1(ground),col2(ground),col3(ground)])],
                   R).

test_rel_pk(R) :-
        rel_create([[a,b,c],[d,e,f]],
                   [coldefs([col1(ground),col2(ground),col3(ground)]),
                    pks([col1])],
                    R).

test_rel_pk_large(R) :-
        rel_create([[a,1,1],
                    [b,1,2],
                    [c,3,4],
                    [d,4,1],
                    [e,1,1],
                    [f,1,2]],
                   [coldefs([col1(ground),col2(ground),col3(ground)]),
                    pks([col1])],
                    R).

test(rel_create) :-
        rel_create(R),
        rel_count(R, 0).

test(rel_insert) :-
        rel_create([[a,b,c],[d,e,f]], R),
        rel_count(R, 2),
        rel_insert(R, [g,h,i], R2),
        rel_count(R2, 3),
        rel_insert(R, [a,b,c], R3),
        rel_count(R3, 2).

test(rel_create) :-
        test_rel(R),
        rel_count(R, 2),
        rel(Meta,_) = R,
        meta_arity(Meta, Arity),
        Arity = 3,
        meta_names(Meta, Names),
        Names = [col1,col2,col3],
        meta_preds(Meta, Preds),
        Preds = [ground,ground,ground],
        meta_pkidxs(Meta, PKIdxs),
        PKIdxs = [].

test(rel_create) :-
        test_rel_pk(R),
        rel(Meta,_) = R,
        meta_pkidxs(Meta, PKIdxs),
        PKIdxs = [0].

test(primarykeys) :-
        test_rel_pk(R),
        rel_count(R, 2),
        rel_insert(R, [a,d,d], R2),
        rel_count(R2, 2).

test(primarykeys) :-
        test_rel(R),
        rel_count(R, 2),
        rel_insert(R, [a,d,d], R2),
        rel_count(R2, 3).

test(rel_query) :-
        test_rel(R),
        findall([Y,Z], rel_query(R, [_,Y,Z]), Result),
        Result = [[b,c],[e,f]],
        findall([Y,Z], rel_query(R, [a,Y,Z]), Result2),
        Result2 = [[b,c]].

test(rel_query) :-
        test_rel_pk(R),
        findall([Y,Z], rel_query(R, [_,Y,Z]), Result),
        Result = [[b,c],[e,f]],
        findall([Y,Z], rel_query(R, [a,Y,Z]), Result2),
        Result2 = [[b,c]].

test(rel_delete) :-
        test_rel(R),
        rel_count(R, 2),
        rel_delete(R, [a,b,d], R1),
        rel_count(R1, 2),
        rel_delete(R, [a,b,c], R2),
        rel_count(R2, 1).

test(rel_delete) :-
        test_rel_pk(R),
        rel_count(R, 2),
        rel_delete(R, [a,b,d], R1),
        rel_count(R1, 2),
        rel_delete(R, [a,b,c], R2),
        rel_count(R2, 1).

test(rel_delete) :-
        test_rel_pk_large(R),
        rel_count(R, 6),
        rel_delete(R, [a,_,_], R1),
        rel_count(R1, 5),
        rel_delete(R, [_,1,_], R2),
        rel_count(R2, 2),
        rel_delete(R, [_,3,_], R3),
        rel_count(R3, 5).

test(rel_select) :-
        test_rel(R),
        rel_count(R, 2),
        rel_select(R, [a,b,d], R1),
        rel_count(R1, 0),
        rel_select(R, [a,b,c], R2),
        rel_count(R2, 1),
        findall(X, rel_query(R2, X), Xs),
        Xs = [[a,b,c]].

test(rel_select) :-
        test_rel_pk(R),
        rel_count(R, 2),
        rel_select(R, [a,b,d], R1),
        rel_count(R1, 0),
        rel_select(R, [a,b,c], R2),
        rel_count(R2, 1),
        findall(X, rel_query(R2, X), Xs),
        Xs = [[a,b,c]].

test(rel_select) :-
        test_rel_pk_large(R),
        rel_count(R, 6),
        rel_select(R, [a,_,_], R1),
        rel_count(R1, 1),
        rel_select(R, [_,1,_], R2),
        rel_count(R2, 4),
        rel_select(R, [_,3,_], R3),
        rel_count(R3, 1).
        
:- end_tests(relation).